(ns quakelist.dev
  (:require [clojure.repl :refer :all :exclude [pst]]
            [clojure.tools.namespace.repl :refer [clear refresh refresh-all]]
            [clojure.pprint]
            [mount.core]
            [quakelist.figwheel :refer :all]
            [quakelist.infra.main :refer :all]
            [quakelist.infra.util :refer :all]))

(comment
  ; after ns move
  (clear))

(intern 'clojure.core 'pprint clojure.pprint/pprint)
(intern 'clojure.core 'pst clojure.repl/pst)

(if (System/getenv "DISPLAY")
  (do (require 'inspector-jay.core)
      (def inspect (ns-resolve 'inspector-jay.core 'inspect)))
  (defn inspect [x]
    (spy x)))

(intern 'clojure.core 'inspect inspect)

(clojure.tools.namespace.repl/set-refresh-dirs "src")

(set! *warn-on-reflection* true)
(set! clojure.core/*print-length* 150)
(set! clojure.core/*print-level* 10)

(defn stop [& args]
  (apply mount.core/stop args))

(defn reset []
  (stop)
  (refresh-all :after `start))

(defn init []
  (if (System/getenv "AUTORUN")
    (refresh :after `start)))

(defstate* figwheel-dev
  :start (figwheel)
  :stop (stop-figwheel))

(comment
  (inspect (sort-by (comp :order val) @@#'mount.core/meta-state)))
