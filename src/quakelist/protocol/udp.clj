(ns quakelist.protocol.udp
  (:refer-clojure :exclude [send])
  (:import (java.net InetAddress DatagramPacket DatagramSocket)
           (java.nio.charset Charset))
  (:require [quakelist.infra.util :refer :all]))

(def- default-so-timeout 10000)

; sysctl -w net.core.rmem_max=8388608
; sysctl -w net.core.wmem_max=8388608
(defn ^DatagramSocket socket
  ([] (socket default-so-timeout))
  ([timeout-ms] (doto (DatagramSocket.)
                  (.setReceiveBufferSize (* 24 1024 1024))
                  (.setSendBufferSize (* 4 1024 1024))
                  (.setSoTimeout timeout-ms))))

(defn- packet
  ([^bytes data addr ^long port]
   (DatagramPacket. data (alength data) (InetAddress/getByName addr) port))
  ([len] (DatagramPacket. (byte-array len) len)))

(def- max-packet-size 2000)

(defn- ^DatagramPacket raw-receive [^DatagramSocket socket]
  (doto>> (packet max-packet-size)
    (.receive socket)))

(defn receive [socket]
  (let [res (raw-receive socket)
        addr (.getAddress res)]
    {;:host (.getCanonicalHostName addr)
     :addr (.getHostAddress addr)
     :port (.getPort res)
     :data (.getData res)}))

(defn send [^DatagramSocket socket addr port data]
  {:pre [socket addr port data]}
  (.send socket (packet data addr port)))
