(ns quakelist.protocol.qlserver
  (:import [java.io ByteArrayOutputStream]
           [java.nio ByteBuffer ByteOrder])
  (:require [quakelist.infra.util :refer :all]))

; https://developer.valvesoftware.com/wiki/Server_queries

(defn- ^ByteBuffer bytebuf [size]
  (doto (ByteBuffer/allocate size) (.order ByteOrder/LITTLE_ENDIAN)))

(defn- wrapbuf [from]
  (doto (ByteBuffer/wrap from) (.order ByteOrder/LITTLE_ENDIAN)))

(defn- get-ubyte [^ByteBuffer buf]
  (Byte/toUnsignedLong (.get buf)))

(defn- get-ushort [^ByteBuffer buf]
  (Short/toUnsignedLong (.getShort buf)))

(defn- get-uint [^ByteBuffer buf]
  (Integer/toUnsignedLong (.getInt buf)))

(defn- get-string [^ByteBuffer buf]
  (loop [res (ByteArrayOutputStream.)
         b (get-ubyte buf)]
    (if (zero? b)
      (.toString res "utf-8")
      (recur (doto res (.write (int b)))
             (get-ubyte buf)))))

(def- request-type
  {:info 0x54
   :players 0x55
   :rules 0x56})

(def- response-type
  {0x41 :challenge
   0x44 :players
   0x45 :rules
   0x49 :info})

(defmulti request-data (fn [type challenge] type))

(defn- buf->array ^bytes [^ByteBuffer buf]
  (.array buf))

(def- info-data
  (buf->array (doto (bytebuf 25)
                (.putInt -1)
                (.put (byte (request-type :info)))
                (.put (.getBytes "Source Engine Query" "ASCII"))
                (.put (byte 0x00)))))

(defmethod request-data :info [_ challenge]
  (if challenge
    (buf->array (doto (bytebuf (+ 4 (count info-data)))
                  (.put ^bytes info-data)
                  (.putInt challenge)))
    info-data))

(defmethod request-data :players [_ challenge]
  (buf->array (doto (bytebuf 9)
                (.putInt -1)
                (.put (byte (request-type :players)))
                (.putInt (or challenge -1)))))

(defmethod request-data :rules [_ challenge]
  (buf->array (doto (bytebuf 9)
                (.putInt -1)
                (.put (byte (request-type :rules)))
                (.putInt (or challenge -1)))))

(defn parse-header [^ByteBuffer buf]
  (case (.getInt buf)
    -1 (hash-map :format :single
                 :type (response-type (get-ubyte buf)))
    -2 (hash-map :format :multi
                 :id (get-uint buf)
                 :total (get-ubyte buf)
                 :number (get-ubyte buf)
                 :header (response-type (get-ubyte buf)))))

(defmulti parse-data (fn [hdr buf] (:type hdr)))

(defmethod parse-data :info [hdr ^ByteBuffer buf]
  (hash-map :protcol (get-ubyte buf)
            :name (get-string buf)
            :mapname (get-string buf)
            :folder (get-string buf)
            :game (get-string buf)
            :id (get-ushort buf)
            :players (get-ubyte buf)
            :maxplayers (get-ubyte buf)
            :bots (get-ubyte buf)
            :servertype (case (char (.get buf))
                          \d :dedicated
                          \l :nondedicated
                          \p :proxy)
            :environment (case (char (.get buf))
                           \l :linux
                           \w :windows
                           \o :macO
                           \m :macM)
            :visibility (if (zero? (.get buf))
                          :public
                          :private)
            :vac (if (zero? (.get buf))
                   :unsecured
                   :secured)
            :version (get-string buf)))

(defmethod parse-data :challenge [hdr ^ByteBuffer buf]
  (.getInt buf))

(defn- parse-player [^ByteBuffer buf]
  (hash-map :index (.get buf)
            :name (get-string buf)
            :score (.getInt buf)
            :duration (double (.getFloat buf))))

(def- player-order (juxt (comp - :score) (comp - :duration)))

(defmethod parse-data :players [hdr buf]
  (->> (repeatedly (get-ubyte buf) #(parse-player buf))
       (sort-by player-order)
       vec))

(defn- parse-rule [buf]
  (map-entry (keyword (get-string buf)) (get-string buf)))

(def gametypes {0 :ffa
                1 :duel
                2 :race
                3 :tdm
                4 :ca
                5 :ctf
                6 :fctf
                7 :obelisk
                8 :harvester
                9 :freezetag
                10 :domination
                11 :attack-defend
                12 :red-rover})

(defmethod parse-data :rules [hdr buf]
  (-> (into {} (repeatedly (get-ushort buf) #(parse-rule buf)))
      (update :teamsize parse-int)
      (update :sv_maxclients parse-int)
      (update :g_gameState keyword)
      (update :bot_minplayers parse-int)
      (update :g_needpass #(not= "0" %))
      (update :g_gametype #(or (gametypes (parse-int %))
                               (keyword (doto>> %
                                          (warn "unknown gametype:")))))))

(defn parse-packet [data]
  (try
    (let [buf (wrapbuf data)
          hdr (parse-header buf)]
      (assert (= :single (:format hdr)) "multi packets unimplemented")
      {:header hdr
       :data (parse-data hdr buf)})
    (catch Throwable e
      (throw (ex-info "failed to parse packet" {:data (hex data)} e)))))
