(ns quakelist.protocol.steamwebapi
  (:require [quakelist.infra.util :refer :all]))

; https://steamapi.xpaw.me/#IGameServersService/GetServerList
; https://developer.valvesoftware.com/wiki/Master_Server_Query_Protocol#Filter

; curl 'https://api.steampowered.com/IGameServersService/GetServerList/v1/?key=$STEAM_WEBAPI_KEY&format=json&filter=\appid\282440'

; https://steamcommunity.com/dev/apikey

(def- url (str "https://api.steampowered.com/IGameServersService/GetServerList/v1/"))
(def- appid-quake-live 282440)
(def- query (str "\\appid\\" appid-quake-live #_"\\empty\\0"))

(defn- get-key []
  (or (System/getenv "STEAM_WEBAPI_KEY")
      (throw (RuntimeException. "STEAM_WEBAPI_KEY environment variable is not set"))))

(defn- get-limit []
  (or (some-> (System/getenv "MAX_SERVERS") parse-int)
      4000))

(def- full-url (delay (str url "?format=json&filter=" query "&key=" (get-key) "&limit=" (get-limit))))

(defn get-server-list []
  (-> @full-url slurp parse-json :response :servers))

(comment
  (inspect (get-server-list)))
