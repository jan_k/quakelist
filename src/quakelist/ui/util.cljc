(ns quakelist.ui.util
  #?(:cljs (:require-macros quakelist.ui.util)))

#?(:clj
   (do
     (defmacro log+ [& args]
       `(js/console.log ~@args))

     (defmacro log [& args]
       `(js/console.debug ~@args))

     (defmacro warn [& args]
       `(js/console.warn ~@args))

     (defmacro spy [arg & args]
       `(doto ~arg (js/console.debug ~@args)))

     (defmacro spy>> [& args]
       `(let [args# [~@args]
              res# (peek args#)]
          (apply (.-log js/console) args#)
          res#)))
   :cljs
   (do
     (some-> js/goog.debug.Console.instance (.setCapturing false))

     (extend-type js/Symbol
       IPrintWithWriter
       (-pr-writer [obj writer _opts]
         (-write writer (.toString obj))))

     (defn set-url! [v]
       (.replaceState js/history {} "" v))

     (defn element-by-id [id]
       (.getElementById js/document (name id)))

     (defn element-by-id! [id]
       (doto (element-by-id id)
         (assert (str "found no element with id " id))))))
