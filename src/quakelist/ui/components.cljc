(ns quakelist.ui.components
  (:require [rum.core :as rum]
            [quakelist.ui.util :as u]
            [quakelist.ui.flags :refer [flag]]
            [quakelist.infra.util :as v]))

(rum/defc +status < rum/reactive
  ([] (+status (delay nil) (delay nil)))
  ([state sente-state]
   (let [{:keys [error started init]} (rum/react state)
         {:keys [open?]} (rum/react sente-state)
         blink? (or error (and started (not open?)))
         color (cond
                 error "red"
                 (and init open?) "green"
                 started "orange"
                 :else "gray")]
     [:div.right
      [:br]
      [:span.status {:class [(if blink? "blink") color]
                     :title (cond
                              error (str "Page probably broke :(\n"
                                         "Try refreshing.\n\n"
                                         error)
                              init "Receiving live updates"
                              open? "Waiting for up-to-date server list"
                              started "Trying to connect for updates"
                              :else "Server connection inactive\nEnable Javascript for live updates")}
       "\u2b24"]])))

(defn- playercount
  [{:keys [players humans spectators maxplayers maxclients bots]}]
  (let [limit (if (and maxplayers maxclients)
                (min maxplayers maxclients)
                (or maxplayers maxclients))
        numplaying (if limit
                     (min humans limit)
                     humans)]
    (str (if (empty? players)
           "Empty server"
           (str (v/join-some " + "
                             [(if (pos? numplaying)
                                (str numplaying " player"
                                     (if (not= 1 numplaying) \s)))
                              (if (pos? spectators)
                                (str spectators " spectator"
                                     (if (not= 1 spectators) \s)))
                              (if (pos? bots) (str bots " bot"
                                                   (if (not= 1 bots) \s)))])))
         (if limit (str " / max " limit " players\n")))))

(defn- players-tooltip [{:keys [players] :as s}]
  (str (playercount s) (v/join "\n" (map :plain-name players))))

(def lock "\ud83d\udd12")
(def globe "\ud83c\udf10")

(def block \u2588)
(def halfblock \u2592)
(def partblock \u2591)
(def space \u200a)
(def warmup "\ud83d\udd50")

(defn- playerbar [{:keys [bots humans spectators teamsize]}]
  (let [<players> (mapcat (fn [[p1 p2]] (if p2 [p1 (conj p2 space)] [p1]))
                          (->> (repeat (- humans spectators)
                                       [:span.bp block space])
                               (partition-all 2)))
        <bots> (repeat bots [:span.bb partblock space])
        <specs> (repeat spectators [:span.bs halfblock space])]
    [:div.playerbar
     (v/for-indexed [[i p] (->> (concat <players> <bots> <specs>)
                                (partition-all 2))]
       [:span {:key i} [:span.nb p] [:wbr]])]))

(defn- lock-tooltip [{:keys [password]}]
  (if password
    "Password protected"
    "Open server"))

(defn- players-td [{:keys [maxplayers humans] :as s}]
  [:td (cond-> {:title (players-tooltip s)}
         (and maxplayers (<= maxplayers humans)) (assoc :class "full"))
   (playerbar s)])

(defn- server-tooltip [{:keys [tags] :as s}]
  (str (:name s) "\n"
       (if tags (str "Tags: " (v/join ", " tags) "\n"))
       "Version: " (:version s)))

(defn filter-match [s {:keys [region gametype hide] :as filters}]
  (and (or (empty? region) (region (:continent (:country s))))
       (or (empty? gametype) (gametype (:gametype s)))))

(defn full-filter-match [s {:keys [region gametype hide] :as filters}]
  (and (filter-match s filters)
       (or (not (:empty hide))
           (pos? (:humans s 0)))
       (or (not (:full hide))
           (nil? (:maxplayers s))
           (< (count (:players s)) (:maxplayers s)))
       (or (not (:private hide)) (not (:password s)))))

(def gametypes
  (sorted-map
   :ffa "FFA"
   :duel "Duel"
   :race "Race"
   :tdm "TDM"
   :ca "CA"
   :ctf "CTF"
   :freezetag "FT"
   :domination "DOM"
   :attack-defend "AD"
   :red-rover "RR"))

(def gametype-labels
  {:ffa "Free For All"
   :duel "Duel"
   :race "Race"
   :tdm "Team Deathmatch"
   :ca "Clan Arena"
   :ctf "Capture the Flag"
   :freezetag "Freeze Tag"
   :domination "Domination"
   :attack-defend "Attack & Defend"
   :red-rover "Red Rover"})

(def gametypes* (v/map-invert gametypes))

(defn filter-servers [servers filters]
  (filter #(filter-match (val %) filters) servers))

(defn filter-servers-full [servers filters]
  (filter #(full-filter-match (val %) filters) servers))

(defn- display-map [mapname]
  (v/lower-case (cond-> mapname
                  (< 20 (count mapname)) (subs 1 20))))

(defn- minutes-between [nxt ts]
  (v/round (/ (- nxt ts) (* 1000 60))))

(defn- display-minutes [mins incomplete?]
  (cond
    (nil? mins) "current"
    (< mins 3) (if incomplete? "time unknown" "briefly")
    (< 300 mins) "long time"
    :else (str mins (if incomplete? \+) " minutes")))

(defn- map-history [history cts]
  (if (seq history)
    (str "Most recent maps:\n"
         (v/join "\n"
                 (reverse
                   (for [[{:keys [ts mapname incomplete]} {nxt :ts}]
                         (map vector history (concat (next history) [nil]))
                         :when (or (nil? nxt) (< (minutes-between cts ts) 120))
                         :let [mins (some-> nxt (minutes-between ts))]]
                     (str mapname
                          " (" (display-minutes mins incomplete) ")")))))))

(rum/defc +list < rum/reactive [state]
  (let [{:keys [servers filters snapshot]} (rum/react state)
        showtype (< 1 (count (:gametype filters)))
        showlock (not (:private (:hide filters)))
        cols (cond-> 8
               (not showlock) dec
               (not showtype) dec)
        filtered-servers (filter-servers-full servers filters)]
    (assert (or (nil? servers) (sorted? servers)))
    [:table
     [:colgroup (concat (if showlock
                          [[:col {:key "lock"}] [:col.narrow {:key "y"}]]
                          [[:col.narrow {:key "y"}] [:col {:key "x"}]])
                        (map #(vector :col {:key %}) (range (- cols 2))))]
     [:thead
      [:tr
       (if showlock [:th {:title "Password protection status"} lock])
       [:th.pl (v/join (concat (repeat 4 \u00A0) "Players" (repeat 4 \u00A0)))]
       [:th.nb "State"]
       [:th {:title "Location"} globe]
       [:th "Server name"]
       (if showtype [:th "Type"])
       [:th "Map"]
       [:th "Address"]]]
     [:tbody
      (if (empty? filtered-servers)
        [:tr [:td.none {:colSpan cols} "No servers to display"]]
        (for [[id {:keys [mapname country gamestate] :as s}] filtered-servers]
          [:tr {:key id}
           (if showlock
             [:td.center {:title (lock-tooltip s)} (if (:password s) lock)])
           (players-td s)
           (if (= :PRE_GAME gamestate)
             [:td.center.orange
              {:title "Warmup / match not yet started"} warmup]
             (if-let [{:keys [red blue]} (:score s)]
               [:td.center.nb
                [:span.red.b red]
                [:span.b.gray "\u2009:\u2009"]
                [:span.blue.b blue]]
               [:td.center.dj {:title "Initializing"} [:span.orange \u25B6]]))
           [:td.center {:title (:name country)} (flag (:code country))]
           [:td.wrap {:title (server-tooltip s)} (:name s)]
           (if showtype [:td.center {:title (gametype-labels (:gametype s))}
                         (gametypes (:gametype s))])
           [:td {:title (if (pos? (:humans s))
                          (map-history (:map-history s) snapshot))}
            (display-map mapname)]
           [:td [:a {:href (str "steam://connect/" id)} id]]]))]]))

(defn- set-toggle [s v]
  {:post [(set? %)]}
  (if (contains? s v)
    (disj s v)
    (conj (or s (sorted-set)) v)))

(def regions
  (sorted-map
   ;:AN "Antarctica"
   :AS "Asia"
   :OC "Oceania"
   :AF "Africa"
   :RU "Russia"
   :SA "South America"
   :NA "North America"
   :EU "Europe"))

(def regions_ (v/map-vals #(v/replace-first % \space \_) regions))
(def regions* (v/map-invert regions))

(def hides
  (sorted-map
   :empty "empty"
   :full "full"
   :private "private"))

(def hides* (v/map-invert hides))

(def themes (sorted-map :auto "default" :dark "dark" :light "light"))

(defn make-url [{:keys [region gametype hide theme] :as filters}]
  (str \/ (v/join \/ (map #(v/join "+" %)
                          (concat [(if (= (count region) (count regions))
                                     ["ALL"]
                                     (map regions_ region))
                                   (if (= (count gametype) (count gametypes))
                                     ["ALL"]
                                     (map gametypes gametype))]
                                  (if (seq hide)
                                    [["hide"] (map hides hide)])
                                  (if (and (seq theme) (not= #{:auto} theme))
                                    [(map themes theme)]))))))

(rum/defc +toggle-set < rum/reactive
  [started? state lbl opts tgt reload? multi? all? & [labels]]
  (let [started? (rum/react started?)
        link? (or (not started?) reload?)
        s (rum/react state)
        all (and all? (= (count opts) (count (s tgt))))
        link (if multi?
               #(make-url (update @state tgt set-toggle %))
               #(make-url (assoc @state tgt (if (coll? %) % #{%}))))]
    [:div.pad
     [:label lbl]
     (if all?
       [:a (if-not all {:href (link (keys opts))})
        [:button.all {:type "button"
                      :class (if all "loaded" "disabled")}
         "ALL"]])
     (for [[k lbl] opts
           :let [clickable? (or multi? all (not= (s tgt) #{k}))]]
       [:a #?(:clj (if (and link? clickable?) {:href (link k) :key k})
              :cljs (cond-> {:key k}
                      (and link? clickable?) (assoc :href (link k))))
        [:button
         (cond-> {:type "button"
                  :on-click (fn [_]
                              #?(:cljs
                                 (when (or multi? (not reload?))
                                   (u/set-url! (link k))
                                   (if multi?
                                     (swap! state update tgt set-toggle k)
                                     (swap! state assoc tgt #{k})))))
                  :class (if (or all (not (contains? (s tgt) k)))
                           "disabled"
                           "loaded")}
           labels (assoc :title (labels k)))
         lbl]])]))

(rum/defc +controls [state]
  (let [filters (rum/cursor-in state [:filters])
        s (rum/cursor-in state [:started])]
    [:div
     (+toggle-set s filters "Region" regions :region true false true)
     (+toggle-set s filters "Gametype" gametypes :gametype true false true
                  gametype-labels)
     [:div.stack
      (+toggle-set s filters "Hide" hides :hide false true false)
      (+toggle-set s filters "Theme" themes :theme false false false)]]))

(defn filters [uri]
  (let [uri (if (v/starts-with? uri "/") (subs uri 1) uri)
        [region gametype _ hide] (-> uri
                                     (v/replace-all #"_" " ")
                                     (v/split #"/")
                                     ((v/flip map) #(v/split % #"\+")))
        [region gametype hide] [(apply sorted-set (keep regions* region))
                                (apply sorted-set (keep gametypes* gametype))
                                (apply sorted-set (keep hides* hide))]]
    {:hide hide
     :region (if (or (empty? region) (= #{"ALL"} region))
               (apply sorted-set (keys regions))
               region)
     :gametype (if (or (empty? gametype) (= #{"ALL"} gametype))
                 (apply sorted-set (keys gametypes))
                 gametype)
     :theme (condp #(.endsWith %2 %1) uri
              "/dark" #{:dark}
              "/light" #{:light}
              #{:auto})}))
