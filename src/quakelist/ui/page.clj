(ns quakelist.ui.page
  (:require [rum.core :as rum]
            [clojure.core.memoize :as memo]
            [quakelist.server.websocket :as ws]
            [quakelist.server.http :refer [http-handler]]
            [quakelist.ui.components :as c]
            [quakelist.refresher.server :as r]
            [quakelist.infra.geoip :as geo]
            [quakelist.infra.util :refer :all]))

(def- title (or (System/getenv "WEBSITE_TITLE") "quakelist.net"))

(defn serialize [x]
  (binding [*print-level* nil
            *print-length* nil]
    (pr-str x)))

(defn render-page [filters static?]
  (debug "rendering" (if static? "static" "live") "page")
  (let [{:keys [servers ts]} (r/serverlist-snapshot)
        theme (some-> (first (:theme filters)) name)
        state (atom {:filters filters :servers servers :snapshot ts})]
    (str "<!DOCTYPE html>\n"
         (rum/render-html
          [:html {:lang "en" :data-theme theme}
           [:head
            [:title title]
            [:meta {:name "description"
                    :content "Server browser for Quake Live, a live list of Quake Live servers"}]
            [:link {:href (str "/css/style.css" \? ws/version)
                    :rel "stylesheet" :type "text/css"}]]
           [:body
            [:div#root
             [:h2.hdr "Quake Live server list "]
             [:div.row
              [:div.col1#controls {:data-filters (serialize filters)}
               (c/+controls state)]
              (if-not static? [:div.col2#status (c/+status)])]
             [:div#list {:data-snapshot ts} (c/+list state)]]
            [:p.small [:a {:href "https://gitlab.com/jan_k/quakelist"}
                       "https://gitlab.com/jan_k/quakelist"]]
            (if-not static?
              [:script {:src (str "/js/compiled/client.js?" ws/version)}])]]))))

(def- default-website-cache-ms 10000)

(def- website-cache-ms
  (delay (or (parse-int (System/getenv "WEBSITE_CACHE_MS"))
             default-website-cache-ms)))

(def- cached-page
  (delay (if (pos? @website-cache-ms)
           (memo/ttl render-page {} :ttl/threshold @website-cache-ms)
           render-page)))

(def- all-regions (apply sorted-set (keys c/regions)))

(defn- filters [{:keys [uri remote-addr]}]
  (or (if (#{"" "/"} uri)
        {:region (or (some-> (:continent (geo/lookup remote-addr)) sorted-set)
                     all-regions)
         :hide (sorted-set :empty)
         :gametype (sorted-set :ca)
         :theme #{:auto}})
      (c/filters uri)))

(defn- response [req static?]
  {:body (@cached-page (filters req) static?)
   :headers {"Content-Type" "text/html" "Cache-Control" "no-store"}
   :status 200})

(defmethod http-handler :main [req]
  (info "hit" (:remote-addr req))
  (response req false))

(defmethod http-handler :static [req]
  (info "static hit" (:remote-addr req))
  (response req :static))
