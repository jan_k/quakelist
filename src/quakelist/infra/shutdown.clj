(ns quakelist.infra.shutdown
  {:clojure.tools.namespace.repl/unload false}
  (:require [quakelist.infra.util]
            [mount.core]))

(defonce hook (Thread. (fn []
                         (quakelist.infra.util/warn "shutdown")
                         (mount.core/stop)
                         (quakelist.infra.util/info "shutdown done"))))
