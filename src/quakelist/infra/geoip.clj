(ns quakelist.infra.geoip
  (:import [java.text Normalizer Normalizer$Form])
  (:require [clojure.java.shell :refer [sh]]
            [clojure.core.memoize :as memo]
            [quakelist.infra.util :refer :all]))

; https://location.ipfire.org/

(defn- get-country-list []
  (try
    (-> (sh "location" "list-countries" "--show-name" "--show-continent")
        :out
        (doto (assert "Empty list of countries - libloc DB not updated?"))
        (split #"\n"))
    (catch Throwable e
      (error e "Failed to load country list (missing libloc package?)"))))

; location list-countries --show-name --show-continent > locations
(def locations
  (delay
    (forv [loc (get-country-list)
           :let [[code continent n] (split loc #" " 3)]]
      {:code (keyword code)
       :name n
       :continent (cond
                    (= "AN" continent) :OC ; group antarctica with oceania
                    (= "RU" code) :RU
                    :else (keyword continent))})))

(def countries
  (delay (index-by :name @locations)))

(defn- plaintext [s]
  (-> s
      (Normalizer/normalize Normalizer$Form/NFD)
      (replace-all #"[^\p{ASCII}]| |[,(].*" "")
      lower-case))

(def- server-specific
  {"ukriane" :UA
   "omega" :EU
   "dallas" :US
   "brainwashed" :DE
   "cyberdemon" :GB})

(def- abbr
  {"usa" :US
   "ger" :DE
   "uk" :GB
   "de" :DE
   "fr" :FR
   "nl" :NL})

(def country-tags
  (delay (for-into (let [codes (index-by :code @locations)]
                     (map-vals codes (merge server-specific abbr)))
                   [{:keys [name code] :as loc} @locations
                    :when (not= :FX code)
                    name (split name #" and ")]
           (plaintext name) loc)))

(defn- lookup* [ip]
  (if @locations
    (try
      (@countries (keep-first #(re-first-group #"Country *: (.*)$" %)
                              (split (:out (sh "location" "lookup" ip)) #"\n")))
      (catch Throwable e
        (error e "failed to lookup location for" ip)))))

(def lookup (memo/ttl lookup* :ttl/threshold (* 1000 60 60 24 3)))

(comment
  (lookup "8.8.8.8"))
