(ns quakelist.infra.mount
  (:require [quakelist.infra.log :refer [debug]]
            [taoensso.truss :refer [have]]
            [mount.core :as mount]
            [mount-up.core :as mu]))

(mount/in-cljc-mode)

(defn- log-state-change [{:keys [action name state start stop]}]
  (when (= (= action :up) (instance? mount.core.NotStartedState state))
    (debug (str (case action :up ">> starting" :down ">> stopping") " " name))))

(mu/on-upndown ::this log-state-change :before)
