(ns quakelist.infra.potemkix
  #?(:cljs (:require-macros quakelist.infra.potemkix))
  #?(:clj (:require [potemkin])))

#?(:clj
   (do
     (defmacro if-cljs [t f]
       (if (:ns &env) t f))

     (defmacro -import-def [from-ns def-name]
       (let [from-sym# (symbol (str from-ns) (str def-name))]
         `(def ~def-name ~from-sym#)))

     (defmacro import-cljs [& imports]
       `(do ~@(for [[from-ns & defs] imports
                    d defs]
                `(-import-def ~from-ns ~d))))

     (defmacro import-vars [& args]
       `(if-cljs
         (import-cljs ~@args)
         (potemkin/import-vars ~@args)))

     (defmacro import-all [n]
       `(if-cljs
         (import-vars [~n ~@(for [[sym v] (ns-publics (the-ns n))
                                  :when (not ((some-fn :macro :no-cljs)
                                              (meta v)))]
                              sym)])
         (import-vars [~n ~@(for [[sym v] (ns-publics (the-ns n))]
                              sym)])))))
