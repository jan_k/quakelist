(ns quakelist.infra.util
  #?(:cljs (:require-macros quakelist.infra.util))
  (:require [clojure.string]
            [clojure.set]
            [net.cgrand.xforms]
            [medley.core]
            [taoensso.truss]
            [mount.core]
            [quakelist.infra.potemkix :refer [import-vars import-all]]
            #?(:cljs [tailrecursion.priority-map :as prio]
               :clj [clojure.data.priority-map :as prio])
            #?(:clj [jsonista.core :as json])
            #?(:clj [quakelist.infra.mount])
            #?(:clj [quakelist.infra.log])))

#?(:clj (import-all quakelist.infra.log))
#?(:clj (import-all quakelist.infra.mount))

#?(:clj (import-vars
         [taoensso.truss have have! have? with-dynamic-assertion-data]))

(import-vars
 [clojure.string ends-with? starts-with? join includes? split replace-first lower-case]
 [clojure.set map-invert difference]
 [prio priority-map priority-map-by priority-map-keyfn priority-map-keyfn-by]
 [medley.core find-first map-keys map-kv map-vals map-entry filter-kv filter-vals remove-vals remove-kv remove-keys least greatest deref-swap! deref-reset!])

(defn join-some
  ([coll] (join-some "" coll))
  ([sep coll] (join sep (remove empty? coll))))

(def replace-all clojure.string/replace)

(defn index-by
  "Returns a map from the result of calling f on each item in coll to that item."
  ([f coll] (index-by f {} coll))
  ([f to coll] (into to (map (juxt f identity)) coll)))

(defn re-first-group
  "Return the first capturing group of the first match or whole first match if
  no groups specified"
  [re text]
  (if-let [m (first (re-seq re text))]
    (if (vector? m)
      (nth m 1)
      m)))

(defn keep-first
  ([pred]
   (fn [rf]
     (fn
       ([] (rf))
       ([result] (rf result))
       ([result x]
        (if-let [r (pred x)]
          (ensure-reduced (rf result r))
          result)))))
  ([pred coll]
   (reduce (fn [_ x] (if-let [r (pred x)] (reduced r))) nil coll)))

(defmacro def- [n & forms]
  `(def ~(vary-meta n assoc :private true) ~@forms))

(defmacro doto>> [x & forms]
  (let [gx (gensym)]
    `(let [~gx ~x]
       ~@(map (fn [f]
                (with-meta
                  (if (seq? f)
                    `(~@f ~gx)
                    `(~f ~gx))
                  (meta f)))
              forms)
       ~gx)))

(defmacro xfor [& args]
  `(net.cgrand.xforms/for ~@args))

(defmacro forv [[x coll & binds] body]
  `(into [] (xfor [~x ~'% ~@binds] ~body) ~coll))

(defmacro for-map
  ([[x coll & binds] body]
   `(into {} (xfor [~x ~'% ~@binds] ~body) ~coll))
  ([head body-key body-val]
   `(for-map ~head (map-entry ~body-key ~body-val))))

(defmacro for-into
  ([to [x coll & binds] body]
   `(into ~to (xfor [~x ~'% ~@binds] ~body) ~coll))
  ([to head body-key body-val]
   `(for-into ~to ~head (map-entry ~body-key ~body-val))))

(defmacro for-indexed
  ([[x coll & binds] body]
   `(for [~x (map-indexed vector ~coll) ~@binds] ~body)))

(defn =by [f]
  (fn [& args] (apply = (map f args))))

#?(:cljs (defn- check-nan [x]
           (if (js/isNaN x)
             (throw (ex-info "got #NaN" {:val x}))
             x)))

(defn parse-int [x]
  #?(:cljs (check-nan (some-> x js/parseInt))
     :clj (if x (Long/parseLong x))))

(defmacro while-let [[form tst] & body]
  `(loop []
     (when-let [~form ~tst]
       ~@body
       (recur))))

(defmacro while-some [[form tst] & body]
  `(loop []
     (when-some [~form ~tst]
       ~@body
       (recur))))

(defn now-ms []
  #?(:clj (System/currentTimeMillis)
     :cljs (.getTime (js/Date.))))

(defn round [x]
  #?(:clj (Math/round (double x))
     :cljs (Math/round x)))

(defn flip [f] #(apply f %2 %1 %&))

(defmacro defstate* [sym & args]
  `(mount.core/defstate
     ~(vary-meta sym (flip into) {:on-lazy-start :throw
                                  :on-reload :noop
                                  :private true})
     ~@args))

(defmacro defstate [sym start-form]
  `(defstate* ~sym
     :start (let [c# ~start-form]
              (have map? c# :data (str ~(name sym) " start returned non-map")))
     :stop ((:stop (deref ~sym) #(debug ~(name sym) "no-op stop")))))

#?(:clj
   (defn name-thread [name]
     (.setName (Thread/currentThread) name)))

(defn dissoc-in [m [k & ks]]
  (if ks
    (update m k dissoc-in ks)
    (dissoc m k)))

#?(:clj
   (let [json-mapper (json/object-mapper {:decode-key-fn true})]
     (defn parse-json [s]
       (json/read-value s json-mapper))))

(defn join-address [addr port]
  {:pre [addr port]}
  (str addr \: port))

(defn split-address [addr]
  {:pre [(string? addr)]
   :post [(= 2 (count %))]}
  (update (split addr #":" 2) 1 parse-int))

#?(:clj
   (defn hex [x]
     (cond
       (number? x) (format "0x%x" x)
       (string? x) (hex (.getBytes ^String x))
       :else (join (for [b x] (format "%02x" b))))))

#?(:clj
   (defn unhex [hex]
     (->> (partition 2 hex)
          (mapv (fn [[x y]] (unchecked-byte (Integer/parseInt (str x y) 16))))
          byte-array)))

(defn first-key [x]
  (some-> x first key))

(defn rseq* [x]
  (some-> x rseq))

(defn rlast [x]
  (first (rseq* x)))

(defn last-key [x]
  (some-> x rlast key))

(def server-map (partial priority-map-keyfn (juxt (comp - :humans) :steamid)))
