(ns quakelist.infra.log
  (:require [potemkin]
            [clojure.string :as s]
            [clojure.stacktrace :refer [print-cause-trace]]
            [taoensso.timbre :as timbre]
            [taoensso.timbre.appenders.community.rolling
             :refer [rolling-appender]]))

(potemkin/import-vars
 [timbre error warn info debug trace errorf warnf infof debugf tracef])

(defn- stacktrace [{:keys [?err]}] (with-out-str (print-cause-trace ?err)))

(def ts-pattern "yyyy-MM-dd'T'HH:mm:ss.SSSXXX")

(defn- wrap [x] (s/replace-first x #"] - " "]\n"))

(timbre/merge-config!
 {:timestamp-opts {:timezone (java.util.TimeZone/getDefault)
                   :pattern ts-pattern}
  :output-fn #(wrap (timbre/default-output-fn {:error-fn stacktrace} %))
  :appenders {:println {:min-level :debug}
              :rolling (-> {:path "log/quakelist.log"}
                           rolling-appender
                           (assoc :min-level :debug
                                  :async? true))}
  :min-level [["io.netty.*" :warn]
              ["taoensso.sente" :warn]
              ["io.methvin.watcher.*" :warn]
              ["org.eclipse.jetty.*" :warn]
              ["quakelist.refresher.server" :debug]
              ["quakelist.server.websocket" :debug]
              ["*" :debug]]})

(defmacro spy
  ([expr] `(timbre/spy ~expr))
  ([name expr] `(timbre/spy :debug ~name ~expr))
  ([level name expr] `(timbre/spy ~level ~name ~expr)))

(defmacro spy>
  ([expr] `(timbre/spy ~expr))
  ([expr name] `(timbre/spy :debug ~name ~expr))
  ([expr level name] `(timbre/spy ~level ~name ~expr)))

(defmacro logged-future [& args]
  `(timbre/logged-future
     (try
       ~@args
       (catch InterruptedException ~'_))))
