(ns quakelist.infra.main
  (:gen-class)
  (:require [quakelist.infra.util :refer :all]
            [quakelist.infra.shutdown :as shutdown]
            [quakelist.refresher.server]
            [quakelist.refresher.master]
            [quakelist.server.http]
            [quakelist.ui.page]
            [mount.core]))

(defmethod print-method clojure.lang.PersistentQueue [x w]
  (print-method (seq x) w))

(def- report
  (delay
   (let [p (java.lang.ProcessHandle/current)]
     (some-> (.. p info command (orElse nil)) debug)
     (debug "$" (.. p info commandLine (orElse ""))
            (seq (.. p info arguments (orElse ""))))
     (debug "PID =" (.pid p)))
   (debug "*assert* =" *assert*)
   (debug "launch order:" (->> @@#'mount.core/meta-state
                               (sort-by (comp :order val))
                               (mapv key)
                               (join ", ")))))

(defn start []
  (Thread/setDefaultUncaughtExceptionHandler
   (reify Thread$UncaughtExceptionHandler
     (uncaughtException [_ thread ex]
       (error ex "Uncaught exception on thread" (.getName thread)))))
  @report
  (try
    (.addShutdownHook (Runtime/getRuntime) shutdown/hook)
    (catch IllegalArgumentException _ (comment "hook already set")))
  (mount.core/start))

(defn -main []
  (start))
