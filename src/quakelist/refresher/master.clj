(ns quakelist.refresher.master
  (:require [quakelist.infra.util :refer :all]
            [quakelist.protocol.steamwebapi :as steam]))

(def- default-master-refresh-interval-ms (* 120 1000))

(def- refresh
  (delay (or (some-> (System/getenv "MASTER_REFRESH_INTERVAL_SEC")
                     (-> parse-int (max 10) (* 1000)))
             default-master-refresh-interval-ms)))

(defn master-refresh-interval [] @refresh)

(defn- make-refresher [state]
  (let [refresh (master-refresh-interval)]
    (while true
      (try
        (let [servers (steam/get-server-list)]
          (infof "got %d servers from master" (count servers))
          (reset! state {:servers servers
                         :ts (now-ms)}))
        (catch InterruptedException e (throw e))
        (catch Throwable e
          (warn e "failed to load servers from master")))
      (Thread/sleep refresh))))

(defstate master-refresher
  (let [state (atom {:servers nil} :validator #(contains? % :servers))
        f (logged-future
            (name-thread "master refresher")
            (make-refresher state))]
    {:state state
     :stop #(future-cancel f)}))

(defn current-masterlist []
  @(:state @master-refresher))

(defn watch-masterlist [k f]
  (add-watch (:state @master-refresher) k (fn [_ _ old cur] (f old cur))))

(comment
  (inspect (current-masterlist)))
