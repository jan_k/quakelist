(ns quakelist.refresher.server
  (:require [clojure.core.memoize :as memo]
            [quakelist.infra.util :refer :all]
            [quakelist.infra.geoip :as geo]
            [quakelist.refresher.master :refer :all]
            [quakelist.protocol.qlserver :as ql]
            [quakelist.protocol.udp :as udp]))

(defn- plain-name [x]
  (replace-all x #"\^." ""))

(defn- postproc-player [p]
  (-> p
      (dissoc :index)
      (assoc :plain-name (plain-name (:name p)))))

(defn- postproc-score [{r :g_redScore b :g_blueScore}]
  (let [r (if (seq r) (parse-int r))
        b (if (seq b) (parse-int b))]
    (if-not (or (nil? r) (neg? r) (nil? b) (neg? b))
      {:red r
       :blue b})))

(def- ghost-cutoff 2500)

; workaround for glitched server infos (known problem with QL servers)
(defn- ghost? [{:keys [name duration score]}]
  (and (< ghost-cutoff duration)
       (zero? score)
       (or (empty? name) (starts-with? name "(Bot) "))))

(defn- tagged-country [tags country]
  (or (keep-first @geo/country-tags (map lower-case tags))
      (if (and tags (or (tags "EU") (tags "eu"))
               (not= :EU (:continent country)))
        {:name "European Union" :code :EU :continent :EU})
      country))

(defn- proc-tags [gametype]
  (some-> gametype (split #"\s*,\s*") set))

(defn- postproc
  [{:keys [country master info players ts rules map-history]}]
  (let [{ghosts true nonghosts false} (group-by ghost? players)
        tsz (:teamsize rules)
        tags (proc-tags (:gametype master))
        teamsize (if (and tsz (pos? tsz)) tsz)
        missing (max 0 (- (:bots info) (count nonghosts)))
        playerinfo (mapv postproc-player
                         (concat nonghosts (take missing ghosts)))
        minbots (or (:bot_minplayers rules) 0)
        bots (max (:bots info 0)
                  (min (count playerinfo) (- minbots (count playerinfo)))
                  (count (filter #(starts-with? (:plain-name %) "(Bot)")
                                 playerinfo)))]
    (conj (dissoc info :id :folder :protcol :version :servertype)
          {:ts ts
           :map-history (seq map-history)
           :password (:g_needpass rules)
           :version (or (:version rules) (:version info))
           :country (tagged-country tags country)
           :master-ts (:ts master)
           :tags tags
           :steamid (:steamid master)
           :addr (:addr master)
           :players playerinfo
           :minbots minbots
           :maxplayers (cond
                         (= :duel (:g_gametype rules)) 2
                         teamsize (* 2 teamsize))
           :humans (max 0 (- (count playerinfo) bots))
           :bots bots
           :teamsize teamsize
           :spectators (if teamsize
                         (max 0 (- (count nonghosts) (* 2 teamsize)))
                         0)
           :maxclients (:sv_maxclients rules)
           :score (postproc-score rules)
           :gametype (:g_gametype rules)
           :gamestate (:g_gameState rules)})))

(defn- updated-map-history
  [{{:keys [mapname]} :info history :map-history} ready ts]
  (let [lst (:mapname (last history))
        mapname (some-> mapname lower-case)]
    (if (= lst mapname)
      history
      (cond-> (if history
                (conj history {:mapname mapname :ts ts})
                (conj clojure.lang.PersistentQueue/EMPTY
                      (cond-> {:mapname mapname :ts ts}
                        (not (realized? ready)) (assoc :incomplete true))))
        (< 8 (count history)) pop))))

(defn- push-update! [state stage id rules]
  (let [ts (now-ms)
        server (get (swap! stage update id
                           #(assoc % :rules rules :ts ts
                                   :map-history (updated-map-history
                                                 % (:ready @state) ts)
                                   :expired false :requested nil))
                    id)]
    (when-let [res (and (or (:master server)
                            (warn "active server gone from master" id))
                        (postproc server))]
      (trace "fully updated" id)
      (swap! state #(-> %
                        (assoc :ts ts :last id :expired nil)
                        (assoc-in [:servers id] res))))))

(def- send-pause
  (delay (or (some-> (System/getenv "SEND_PAUSE_MS")
                     (-> parse-int (min 100)))
             1)))

(defonce sender (agent nil :error-handler (fn [_ e] (error e "send error"))))
(defn- send! [socket addr port data]
  (send-off sender (fn [_]
                     (when-not (.isClosed socket)
                       (udp/send socket addr port data)
                       (Thread/sleep @send-pause)))))

(defn- query-server [stage socket addr port]
  (tracef "querying server %s:%s" addr port)
  (let [server (join-address addr port)]
    (swap! stage update server
           #(assoc % :requested :info :reqtime (now-ms) :challenge false
                   :country (or (:country %)
                                (geo/lookup addr))))
    (send! socket addr port (ql/request-data :info nil))))

(def- default-server-refresh-interval-ms (* 60 1000))

(def- refresh
  (delay (or (some-> (System/getenv "SERVER_REFRESH_INTERVAL_SEC")
                     (-> parse-int (max 10) (* 1000)))
             default-server-refresh-interval-ms)))

(def- slow-refresh
  (delay (or (some-> (System/getenv "INACTIVE_SERVER_REFRESH_INTERVAL_SEC")
                     (-> parse-int (max 10) (* 1000)))
             (* 3 @refresh))))

(defn- pick-servers [{:keys [servers]} [id {:keys [reqtime]}]]
  (or (nil? reqtime)
      (let [s (servers id)]
        (or (pos? (:humans s 0))
            (< @slow-refresh (- (now-ms) reqtime))))))

(defn- refresher-sender [state stage socket ready list-ready]
  (info "waiting for first master list")
  @ready
  (while (and (not (Thread/interrupted)) (not (.isClosed socket)))
    (let [servers (filter #(pick-servers @state %) @stage)]
      (info "updating" (count servers) "servers")
      (doseq [id (some-> (keys servers) shuffle)
              :let [[addr port] (split-address id)]
              :while (not (.isClosed socket))]
        (try
          (Thread/sleep 0)
          (query-server stage socket addr port)
          (catch InterruptedException e (throw e))
          (catch Throwable e
            (warn e "failed to query" id)))))
    (Thread/sleep 10000)
    (deliver list-ready true)
    (await sender)
    (Thread/sleep (- @refresh 10000))))

(defn- refresher-receiver [snapshots state stage socket]
  (while (and (not (Thread/interrupted)) (not (.isClosed socket)))
    (try
      (let [{:keys [addr port data]} (udp/receive socket)
            server (join-address addr port)]
        (try
          (let [{:keys [data header] :as res} (ql/parse-packet data)
                t (:type header)]
            (trace "got" t "from" server)
            (if (= :challenge t)
              (if (:challenge (@stage server))
                (throw (ex-info "challenge response declined" res)))
              (if (not= t (:requested (@stage server)))
                (throw (ex-info "unexpected response" res))))
            (case t
              :challenge (when-let [s (or (:requested (@stage server))
                                          (warn "unexpected challenge from "
                                                server))]
                           (trace "sent" s "w/ challenge to" server)
                           (swap! stage assoc-in [server :challenge] true)
                           (send! socket addr port (ql/request-data s data)))
              :rules (push-update! state stage server data)
              (let [req (case t
                          :info :players
                          :players :rules)]
                (swap! stage update server merge
                       {t data :requested req :expired false :challenge false})
                (trace "sent" req "to" server)
                (send! socket addr port (ql/request-data req nil)))))
          (catch InterruptedException e (throw e))
          (catch clojure.lang.ExceptionInfo e
            (warn e "failed updating server info" server))
          (catch Throwable e
            (error e "failed updating server info" server))))
      (catch InterruptedException e (throw e))
      (catch Throwable e
        (warn e "receiver error")
        (Thread/sleep 1000)))))

(def master-expiry-ms (delay (long (* 3 (master-refresh-interval)))))
(def refresh-expiry-ms (delay (long (* 3 @slow-refresh))))

(defn- update-list [stage {:keys [ts servers]}]
  (try
    (reduce (fn [r {:keys [addr] :as v}]
              (update r addr assoc :expired false
                      :master (assoc v :ts ts)))
            (reduce (fn [r addr]
                      (update-in r [addr :expired] #(or % ts)))
                    stage
                    (difference (set (keys stage))
                                (set (map :addr servers))))
            servers)
    (catch Throwable e
      (error e "unrecoverable: failed to process server list update")
      (System/exit 1))))

(defn- expired [servers]
  (keep (fn [[id {:keys [expired ts]}]]
          (if (or (and expired (< @master-expiry-ms (- (now-ms) expired)))
                  (and ts (< @refresh-expiry-ms (- (now-ms) ts))))
            id))
        servers))

(defn- handle-expiry [stage state cur]
  (when-let [exp (and (< @refresh-expiry-ms (- (now-ms) (:start @state)))
                      (-> cur expired seq))]
    (debug "servers expiring" exp)
    (swap! stage (flip apply) dissoc exp)
    (swap! state #(-> %
                      (update :servers (flip apply) dissoc exp)
                      (assoc :expired exp
                             :ts (now-ms)
                             :last nil)))))

(def persist-file (System/getenv "PERSIST_FILE_PATH"))

(defn- persist-serverlist [state]
  (when persist-file
    (try
      (spit persist-file (binding [*print-length* nil
                                   *print-level* nil]
                           (pr-str (select-keys state [:servers :ts]))))
      (catch Throwable e
        (error e "failed to persist server list")))))

(defn- load-server [{:keys [map-history] :as v}]
  (cond-> v map-history
          (update :map-history #(into clojure.lang.PersistentQueue/EMPTY %))))

(defn- load-serverlist []
  (when persist-file
    (try
      (let [{:keys [ts servers]} (read-string (slurp persist-file))]
        (if (< (- (now-ms) @refresh-expiry-ms) ts)
          {:ts ts :servers (reduce-kv (fn [r k v]
                                        (assoc r k (load-server v)))
                                      (server-map)
                                      servers)}
          (info "not using persisted serverlist - too old:" ts)))
      (catch java.io.FileNotFoundException e
        (info "not using persisted serverlist - file not found:" persist-file))
      (catch Throwable e
        (warn e "failed to load persisted server list")))))

(defstate server-refresher
  (let [list-ready (promise) ; first refresh of all servers done
        state (atom {:servers (server-map) :ready list-ready :start (now-ms)}
                    :validator map?)
        stage (atom {} :validator map?)
        snapshots (atom (sorted-map) :validator sorted?)
        ready (promise) ; first master refresh done
        socket (udp/socket 0)
        sender (logged-future
                 (name-thread "server refresher sender")
                 (refresher-sender state stage socket ready list-ready))
        receiver (logged-future
                   (name-thread "server refresher receiver")
                   (refresher-receiver snapshots state stage socket))]
    (when-let [restored (load-serverlist)]
      (info "using persisted serverlist")
      (swap! stage conj (reduce-kv (fn [r k v]
                                     (assoc r k (select-keys v [:ts])))
                                   nil
                                   (:servers restored)))
      (swap! state conj restored)
      (deliver list-ready true))
    (watch-masterlist ::k (fn [old cur]
                            (handle-expiry stage
                                           state
                                           (swap! stage update-list cur))
                            (deliver ready true)))
    (if-not (empty? (:servers (current-masterlist)))
      (deliver ready true))
    {:state state
     :stage stage
     :socket socket
     :snapshots snapshots
     :ready list-ready
     :stop (fn []
             (deliver ready true)
             (deliver list-ready true)
             (future-cancel sender)
             (future-cancel receiver)
             (.close socket)
             (persist-serverlist @state))}))

(defn await-list []
  @(:ready @server-refresher))

(defn watch-serverlist [k f]
  (add-watch (:state @server-refresher) k (fn [_ _ old cur] (f old cur))))

(defn current-serverlist []
  (select-keys @(:state @server-refresher) [:ts :servers]))

(defn- snapshot [s {:keys [ts servers] :as state}]
  (if (and ts (or (empty? s) (<= (+ (last-key s) 10000) ts)))
    (cond-> (assoc s ts servers)
      (< 15 (count s)) (dissoc (first-key s)))
    s))

(defn serverlist-snapshot
  ([snap]
   {:pre [(have? number? snap)]}
   (let [{:keys [snapshots state]} @server-refresher
         snaps (swap! snapshots #(if (seq %) % (snapshot % @state)))]
     (if-let [s (snaps snap)]
       {:servers s
        :ts snap}
       (reduce (fn find-last [res [ts s]]
                 (if (< snap ts)
                   (reduced res)
                   {:servers s
                    :ts ts}))
               nil
               snaps))))
  ([] (await-list)
      (let [{:keys [snapshots state]} @server-refresher
            [ts s] (rlast (swap! snapshots snapshot @state))]
        {:servers s
         :ts ts})))

(comment
  (let [server "198.245.62.186:27961"
        [addr port] (split-address server)]
    (def addr addr)
    (def port port)

    (defn request [req addr port]
      (with-open [socket (udp/socket 10000)]
        (udp/send socket addr port (ql/request-data req nil))
        (let [{:keys [addr port] :as res} (udp/receive socket)
              {:keys [header data] :as p} (ql/parse-packet (:data res))]
          (assert (#{:single} (:format header)))
          (if (= :challenge (:type header))
            (do (udp/send socket addr port (ql/request-data req data))
                (let [{:keys [data] :as res} (udp/receive socket)]
                  (assert (= port (:port res)))
                  (assert (= addr (:addr res)))
                  (ql/parse-packet data)))
            p)))))

  (inspect (request :players addr port))
  (inspect (request :info addr port))
  (inspect (request :rules addr port))

  (inspect (keep :expired (:servers @(:state @server-refresher))))
  (inspect (update @(:state @server-refresher) :servers (partial take 5))))

(comment
  (def s (select-keys @(:state @server-refresher) [:servers :ts]))
  (persist-serverlist s)
  (def l (load-serverlist))
  (assert (= s l)))
