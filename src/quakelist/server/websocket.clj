(ns quakelist.server.websocket
  (:require [quakelist.server.http :refer [http-handler]]
            [clojure.core.async :as a]
            [quakelist.refresher.server :as refresher]
            [quakelist.ui.components :as c]
            [taoensso.sente :as sente]
            [taoensso.sente.interfaces :refer [pack unpack]]
            [taoensso.sente.server-adapters.http-kit :refer [get-sch-adapter]]
            [quakelist.infra.util :refer :all]))

(defonce version #=(java.lang.System/currentTimeMillis)) ; client will force page reload when reconnecting to a changed version (ie. updated jar on server)

(def- packer @#'sente/default-edn-packer)

(defn- prepack [x]
  (pack packer x))

(def- packer* (reify taoensso.sente.interfaces/IPacker
                (pack [o r]
                  (if (keyword? r)
                    (prepack r)
                    (let [[t & x] r]
                      (if (keyword? t)
                        (if (= "server" (namespace t))
                          (str \[ t \space (join \space x) \])
                          (prepack r))
                        (str \[ (join \space (map #(pack o %) r)) \])))))
                (unpack [_ x] (unpack packer x))))

(defn- init-client [send-fn uid]
  (send-fn uid [:server/version version]))

(def- server= (=by #(dissoc % :master-ts :ts)))

(defn- expired-since [old cur]
  (for [[id _] old
        :when (not (contains? cur id))]
    id))

(defn- changed-since [old cur]
  (for [[id o] cur
        :when (not (server= o (old id)))]
    id))

(defn- resume [send-fn uid {:keys [filters] :as q} snap]
  (let [cur (refresher/current-serverlist)]
    (when (not= (:ts cur) (:ts snap))
      (when-let [ids (seq (expired-since (:servers snap) (:servers cur)))]
        (send-fn uid [:server/expire (prepack [ids (:ts cur)])]))
      (doseq [id (changed-since (:servers snap) (:servers cur))
              :let [s ((:servers cur) id)]
              :when (or (c/filter-match ((:servers snap) id) filters)
                        (c/filter-match s filters))]
        (send-fn uid [:server/update (prepack [id s])])))
    (send-fn uid [:server/live (:ts cur)] {:flush? true})))

(defn- update-client-latest [send-fn uid {:keys [filters] :as q}]
  (let [snap (refresher/current-serverlist)]
    (debug "update client" uid "to latest" (:ts snap))
    (send-fn uid [:server/snapshot
                  (prepack (update snap :servers c/filter-servers filters))]
             {:flush? true})
    (send-fn uid [:server/live (:ts snap)] {:flush? true})))

(defn- update-client [send-fn uid {:keys [snapshot filters] :as q}]
  {:pre [(have? number? snapshot)]}
  (if-let [snap (refresher/serverlist-snapshot (:snapshot q))]
    (do (debug "update client" uid "from earlier" snapshot)
        (send-fn uid [:server/snapshot
                      (prepack (update snap :servers c/filter-servers filters))]
                 {:flush? true})
        (resume send-fn uid q snap))
    (update-client-latest send-fn uid q)))

(defn- resume-client [send-fn uid {:keys [snapshot] :as q}]
  {:pre [(have? number? snapshot)]}
  (debug "resume client" uid "since" snapshot)
  (if-let [snap (refresher/serverlist-snapshot snapshot)]
    (resume send-fn uid q snap)
    (update-client-latest send-fn uid q)))

(defn- update-filters [clients uid filters]
  (assoc-in clients [uid :filters] filters))

(defn- process-sente-event
  [chsk {:keys [event id ?data send-fn ring-req uid client-id]} clients]
  (case id
    :chsk/ws-ping (do (trace "sente ping" event)
                      (swap! clients assoc-in [uid :heartbeat] (now-ms)))
    :chsk/uidport-open (do (debug "init client" uid (:remote-addr ring-req))
                           (init-client send-fn uid)
                           (info (count (:any @(:connected-uids chsk)))
                                 "ws clients"))
    :chsk/uidport-close (do (swap! clients dissoc uid)
                            (info (count (:any @(:connected-uids chsk)))
                                  "ws clients"))
    :client/init (do (swap! clients update-filters uid (:filters ?data))
                     (update-client send-fn uid ?data))
    :client/resume (do (swap! clients update-filters uid (:filters ?data))
                       (resume-client send-fn uid ?data))
    (debug "unhandled sente event" event)))

(defn- send-all [send-fn clients msg]
  (doseq [c clients]
    (send-fn c msg)))

(defn- dispatch [send-fn clients old server msg]
  (doseq [[uid {:keys [filters]}] clients
          :when (or (c/filter-match server filters)
                    (c/filter-match old filters))]
    (send-fn uid msg)))

(defn- proc-update [clients send-fn old {:keys [last expired ts servers]}]
  (if expired
    (send-all send-fn (keys clients) [:server/expire (prepack [expired ts])])
    (if-not (server= ((:servers old) last) (servers last))
      (dispatch send-fn clients ((:servers old) last) (servers last)
                [:server/update (prepack [last (servers last)])]))))

(defn- make-ws-server []
  (let [a (agent nil :error-handler (fn [_ e] (error e "sente agent error")))
        clients (atom {})
        exit (a/chan)
        {:keys [ch-recv connected-uids send-fn] :as chsk}
        (sente/make-channel-socket-server!
         (get-sch-adapter) {:user-id-fn #(str (:client-id %) \@ (now-ms))
                            :packer packer*
                            :csrf-token-fn nil})]
    (debug "waiting for updated serverlist")
    (send-off a (fn [_]
                  (refresher/await-list)
                  (debug "got updated serverlist")
                  (refresher/watch-serverlist
                   ::ws #(proc-update @clients send-fn %1 %2))))
    (a/go
      (debug "sente started")
      (while-some [x (a/<! ch-recv)]
        (send a (fn [_] (process-sente-event chsk x clients))))
      (a/close! exit)
      (debug "sente closed"))
    (a/go-loop []
      (a/<! (a/timeout 60000))
      (when (a/alt! exit false :default true)
        (doseq [uid (:any @connected-uids)]
          (send-fn uid [:server/ping])
          (if-let [ts (get-in @clients [uid :heatbeat])]
            (when (< 120000 (- (now-ms) ts))
              (debug "disconnecting" uid)
              (send-fn uid [:chsk/close]))
            (swap! clients update-in [uid :heartbeat] #(or % (now-ms)))))
        (recur)))
    (assoc chsk :stop #(a/close! ch-recv) :clients clients)))

(defstate sente
  (make-ws-server))

(defmethod http-handler :sente [req]
  (case (:request-method req)
    :get ((:ajax-get-or-ws-handshake-fn @sente) req)
    :post ((:ajax-post-fn @sente) req)))

(comment
  (:any @(:connected-uids @sente)))

(comment
  (do (mount.core/stop #'sente)
      (mount.core/stop #'quakelist.server.http/server)
      (mount.core/start #'quakelist.server.http/server)
      (mount.core/start #'sente)))
