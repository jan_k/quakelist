(ns quakelist.server.http
  (:require [org.httpkit.server :as s]
            [ring.middleware.defaults :as rd]
            [ring.middleware.multipart-params.temp-file]
            [ring.util.response :as ring]
            [ring.util.response :as response]
            [quakelist.infra.util :refer :all]))

(defmethod response/resource-data :resource ; for GraalVM native image
  [^java.net.URL url]
  (let [conn (.openConnection url)]
    {:content (.getInputStream conn)
     :content-length (let [len (.getContentLength conn)]
                       (if-not (pos? len) len))}))

(defn- dispatch [{:keys [server-name uri]}]
  (cond
    (some-> server-name  (starts-with? "static.")) :static
    (= "/ws" uri) :sente
    :else :main))

(defmulti http-handler dispatch)

(defmethod http-handler :default [r]
  {:body "This doesn't exist.\n"
   :headers {"Content-Type" "text/plain"}
   :status 404})

(defn- handler [req]
  (try
    ;(inspect req)
    (http-handler req)
    (catch Throwable e
      (error e "http handler error")
      {:status 500
       :headers {"Content-Type" "text/plain"}
       :body "Failed to process request"})))

(def- default-listen-port 8764)

(def- listen-port
  (delay (or (parse-int (System/getenv "HTTP_LISTEN_PORT"))
             default-listen-port)))

(defstate* ^{:on-reload :restart} server
  :start (let [port @listen-port]
           (info "HTTP server starting on" (str "http://localhost:" port))
           (-> handler
               (rd/wrap-defaults (-> rd/site-defaults
                                     (dissoc :session)
                                     (dissoc-in [:security :anti-forgery])))
               (s/run-server {:port port})))
  :stop (@server))
