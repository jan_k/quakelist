(defproject quakelist "1.3.0-SNAPSHOT"
  :jvm-opts ["-Xmx512m" "-Xss512k" "-Dvisualvm.display.name=quakelist"
             "-XX:+HeapDumpOnOutOfMemoryError" "-XX:+ExitOnOutOfMemoryError"
             "-XX:-OmitStackTraceInFastThrow" "-Djdk.attach.allowAttachSelf"
             "-XX:+UnlockDiagnosticVMOptions" "-XX:+DebugNonSafepoints"
             "-Djava.net.preferIPv4Stack=true"]
  :source-paths ["src"]
  :min-lein-version "2.9.1"
  :main quakelist.infra.main
  ;;;;;;;;;;;;;;;;;
  :plugins [[lein-cljsbuild "1.1.7"]
            [lein-aot-order "0.1.0"]
            [lein-with-env-vars "0.2.0"]]
  ;;;;;;;;;;;;;;;;;
  :profiles {:uberjar {:aot :order
                       :main quakelist.infra.main
                       :hooks [leiningen.with-env-vars/auto-inject]
                       :env-vars {:TIMBRE_LOG_LEVEL :debug}
                       :target-path "uberjar-target"
                       :global-vars {*warn-on-reflection* true}
                       :prep-tasks ["compile" ["cljsbuild" "once" "client"]]}
             :dev {:source-paths ["dev"]
                   :dependencies [[com.cemerick/pomegranate "1.1.0"]
                                  [criterium "0.4.6"]
                                  [binaryage/devtools "1.0.7"]
                                  [com.google.guava/guava "24.0-jre"] ; for figwheel 0.2.18
                                  ; explicit jetty version https://figwheel.org/docs/jetty_conflicts.html
                                  [org.eclipse.jetty/jetty-server "9.4.54.v20240208"]
                                  [org.eclipse.jetty.websocket/websocket-servlet "9.4.54.v20240208"]
                                  [org.eclipse.jetty.websocket/websocket-server "9.4.54.v20240208"]
                                  [com.bhauman/figwheel-main "0.2.18"]
                                  [inspector-jay "0.3"]
                                  [cider/piggieback "0.5.3"]
                                  ;[cider/cider-nrepl "0.22.4"]; in profiles.clj
                                  [org.clojure/tools.namespace "1.5.0"]]
                   :repl-options {:nrepl-middleware [cider.piggieback/wrap-cljs-repl]
                                  :port 39693
                                  :init-ns quakelist.dev
                                  :init (init)}
                   :clean-targets ^{:protect false} ["resources/public/js/compiled" "uberjar-target" :target-path]}}
  :dependencies [[com.fzakaria/slf4j-timbre "0.4.1"]
                 [javax.xml.bind/jaxb-api "2.4.0-b180830.0359"]
                 [com.taoensso/timbre "6.5.0"]
                 [com.taoensso/sente "1.17.0"]
                 [com.taoensso/truss "1.11.0"]
                 [tolitius/mount-up "0.1.3"]
                 [metosin/jsonista "0.3.8"]
                 ;[defun "0.3.1"]
                 [org.clojure/data.priority-map "1.2.0"]
                 [tailrecursion/cljs-priority-map "1.2.1-fixd"] ; custom fix for https://github.com/tailrecursion/cljs-priority-map/issues/13
                 ;[com.maxmind.geoip/geoip-api "1.3.1"]
                 [http-kit "2.8.0-RC1"]
                 [ring/ring-defaults "0.4.0"]
                 [medley "1.4.0"]
                 [mount "0.1.15.moje2"] ; custom fix for https://github.com/tolitius/mount/issues/121
                 [net.cgrand/xforms "0.19.6"]
                 [org.clojars.akiel/async-error "0.3"]
                 [org.clojure/clojure "1.11.2"]
                 [org.clojure/clojurescript "1.11.132"]
                 [org.clojure/core.async "1.6.681"]
                 [org.clojure/core.memoize "1.1.266"]
                 [org.slf4j/log4j-over-slf4j "2.0.12"]
                 [potemkin "0.4.7"]
                 [rum "0.12.11"]]
  :cljsbuild {:builds
              [{:id "client"
                :source-paths ["src"]
                :compiler {:output-to "resources/public/js/compiled/client.js"
                           :main quakelist.ui.main
                           :optimizations :advanced
                           :pretty-print false}}]})
